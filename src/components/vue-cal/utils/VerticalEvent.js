import Vue from 'vue'

const defaultEventDuration = 2 // In hours.
const minutesInADay = 24 * 60 // Don't do the maths every time.



var VerticalEvent= (function () {
    function VerticalEvent(event,events,dateUtils) {
        this.event = event || {};
        this.events = events || [];
        this.ud = dateUtils
    };

  VerticalEvent.prototype.getVerticalEventConstants = function getVerticalEventConstants() {
    let VerticalEventConstants = {
      SCHEDULE_TYPE: {
        'stay':{
          'class' : 'stay',
          'flight' : false,
          'flightInfo' : null,
          'stay': true
        },
        'dayoff':{
          'class' : 'dayoff',
          'flight' : false,
          'flightInfo' : null,
          'stay': false
        }

      },
      VerticalStyle: {
        width: '100%',
        marginLeft: '0%',
        float: 'left',
        position: 'absolute',
      },
      MergeStyle: {
        width: '100%',
        marginLeft: '0%',
        float: 'left',
        position: 'absolute',
        top: '0%',
        height: '6px' //min 1
      },
      VerticalClass: 'VerticalEvent',
      MergeClass: 'MergeEvent',
      MarginPercent: 4.16,
    };
    return VerticalEventConstants;
  };

  VerticalEvent.prototype.eventsCount = function eventsCount() {
    return this.events.length
  };



  VerticalEvent.prototype.getStyle= function getStyle(seleteDay) {

    let styleType = this.chkVertical();

    if(!styleType){
      return {}
    }

    let style = this.getStyleDefalut(styleType)

    let isSharing = false;
    if(styleType == 'merge'){
      isSharing = true
    }


    let startHour = this.getStartHour(this.event.start, seleteDay.startDate)
    let width = this.getWidth(this.event.start,startHour,this.event.end, seleteDay.endDate)
    //console.log(isSharing)
    if(isSharing){
      style = this.sharedStyleProcess(style,this.event)
    } else{
      //let beforeWidth = this.getBeforeWidth(this.event,seleteDay,isSharing )
      //startHour = startHour - beforeWidth;
    }


    style.width = this.calcPosition(width) + '%'
    style.marginLeft = this.calcPosition(startHour) + '%'

    return style;
  };


  VerticalEvent.prototype.getStyleDefalut = function getStyleDefalut(styleType) {

    if( styleType == 'vertical'){
      return this.getVerticalEventConstants().VerticalStyle;
    } else if( styleType == 'merge'){
      return this.getVerticalEventConstants().MergeStyle;
    }
    return {}

  };


  VerticalEvent.prototype.calcPosition= function calcPosition(hour) {
    //현재시간 * 4.16 das
    if(hour === 24) {
      return 100;
    }
    return hour * this.getVerticalEventConstants().MarginPercent;
  };


  VerticalEvent.prototype.getStartHour= function getStartHour(eventStartDate,toDay) {

    if( eventStartDate <= toDay){
      return 0;
    }

    let hour = this.ud.formatDate(eventStartDate,'HH')

    return hour;
  };

  VerticalEvent.prototype.getWidth= function getStartPosition(eventStartDate,startHour,eventEndDate,toDayEndDate) {

    let endDate = this.ud.formatDate(eventEndDate,'DD-HH')
    let toEndDate = this.ud.formatDate(toDayEndDate,'DD-HH')
    let startDate = this.ud.formatDate(eventStartDate,'DD-HH')
    if(endDate == startDate){
      return 1;
    }

    let endHour = this.ud.formatDate(eventEndDate,'HH')
    let endMinute = this.ud.formatDate(eventEndDate, 'mm')
    let endSecond = this.ud.formatDate(eventEndDate, 'ss')

    if(toDayEndDate <= eventEndDate){
      endHour = 24
    }

    if(endHour == 23 && endMinute == 59 && endSecond == 59){
      endHour = 24;
    }
    return endHour - startHour;
  };

  VerticalEvent.prototype.getBeforeWidth= function getBeforeWidth(nowEvent,seleteDay,isSharing) {

    let beforeWidth = 0;
    let eventsCount = this.eventsCount()

    if(eventsCount <= 1){
      return beforeWidth;
    }

    let _events = this.events;

    if(isSharing){
      _events = this.getWidthEvents(nowEvent.sharedStyle.MemberKey);
    }


    _events.forEach( _event => {
      if( _event.end <= nowEvent.start ){
        const startHour = this.getStartHour(_event.start, seleteDay.startDate)
        const width = this.getWidth(_event.start,startHour,_event.end, seleteDay.endDate)
        beforeWidth = Number(beforeWidth) + Number(width) + Number(startHour);

      }
    })


    return beforeWidth;
  };

  VerticalEvent.prototype.sharedStyleProcess= function sharedStyleProcess(style,event) {

    //console.log('sharedStyleProcess',event)
    let shredData = event.sharedStyle;

    let total = (shredData.total <= 3 ) ? 3 : shredData.total

    //let topContant = 100 / total;
    let heightContant = 24;
    let individualContant = heightContant / total

    style.top = (individualContant * shredData.sort) + 'px';
    style.height = (heightContant / total) + 'px';
    /*if(event.Type.class === 'stay') {
      let stayHeight = (heightContant / total) / 4;
      style.top = (individualContant * shredData.sort + stayHeight) + 'px';
      style.height = stayHeight * 2 + 'px'
    }*/
    return style;
  };






  VerticalEvent.prototype.getWidthEvents = function getWidthEvents(MemberKey) {

    let sharesData = this.events.reduce(function (r, a) {
      r[a.sharedStyle.MemberKey] = r[a.sharedStyle.MemberKey] || [];
      r[a.sharedStyle.MemberKey].push(a);
      return r;
    }, Object.create(null));

    return sharesData[MemberKey]

  };



  VerticalEvent.prototype.chkVertical= function chkVertical() {


    if(!this.event.class){
      return false;
    }

    let className = this.getVerticalEventConstants().VerticalClass;
    const indexStr = this.event.class.indexOf(className);

    if( indexStr >= 0){
      return 'vertical'
    }

    className = this.getVerticalEventConstants().MergeClass;
    const mergeStr = this.event.class.indexOf(className);

    if( mergeStr >= 0){
      return 'merge'
    }

    return false;
  };

    return VerticalEvent;
})();




export default {
  VerticalEvent: VerticalEvent,
    methods: {

        getMonthStyle(){
            return 'style'
        },

    },
}
